<?php

namespace thvc\Bootstrap;

use Silex\Application;
use Symfony\Component\Yaml\Yaml;

class Bootstrapper
{
    /**
     * @var Application
     */
    private $app;

    /**
     * Bootstrapper constructor.
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }


    /**
     * @return void
     */
    public function routingBootstrapper()
    {
        $yamlArray = Yaml::parse(file_get_contents('../config/routes.yml'));

        foreach ($yamlArray as $routeArray) {
            switch ($routeArray['request_method']) {
                case 'get':
                    $this->app->get($routeArray['route_url'], $routeArray['controller']);
                    break;
                case 'put':
                    $this->app->put($routeArray['route_url'], $routeArray['controller']);
                    break;
                case 'post':
                    $this->app->post($routeArray['route_url'], $routeArray['controller']);
                    break;
            }

        }
    }
}