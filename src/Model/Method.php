<?php

namespace thvc\Model;


class Method
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $method;

    /**
     * Method constructor.
     * @param $id
     * @param $method
     */
    public function __construct($id, $method)
    {
        $this->id = $id;
        $this->method = $method;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }




}
