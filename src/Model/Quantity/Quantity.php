<?php

namespace thvc\Model\Quantity;


abstract class Quantity
{
    /**
     * @var Unit
     */
    protected $unit;

    /**
     * Returns a human readable string representing the quantity
     * @return string
     */
    public abstract function toString();
}
