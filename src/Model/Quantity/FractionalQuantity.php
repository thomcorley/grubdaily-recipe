<?php

namespace thvc\Model\Quantity;


class FractionalQuantity extends Quantity
{
    /**
     * @var int
     */
    protected $numerator;

    /**
     * @var int
     */
    protected $denominator;

    public function toString()
    {
        return $this->numerator . "/" . $this->denominator . $this->unit->toString();
    }
    
}
