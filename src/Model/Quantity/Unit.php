<?php

namespace thvc\Model\Quantity;

use Assert\Assertion;



class Unit
{
    /**
     * @var string
     */
    private $unit;
    /**
     * @var string[]
     */
    private $allowedUnits = [
        'g',
        'ml',
        'L',
    ];



    /**
     * Unit constructor.
     * @param string $unit
     * @throws \Exception
     */
    public function __construct($unit)
    {
        if (! in_array($unit, $this->allowedUnits)) {
            throw new \Exception("
                The unit '" . $unit . "' is not an allowed unit. Allowed units are: " . implode(', ', $this->allowedUnits)
            );
        }

        Assertion::inArray($unit, $this->allowedUnits);
        $this->unit = $unit;
    }

    public function toString()
    {
        return $this->unit;
    }
}
