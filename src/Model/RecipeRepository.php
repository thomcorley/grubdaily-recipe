<?php

namespace thvc\Model;


class RecipeRepository
{
    /**
     * @var \PDO
     */
    protected $db;

    /**
     * RecipeRepository constructor.
     * @param \PDO $db
     */
    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @return Recipe[]
     */
    public function getAll()
    {
        $statement = $this->db->prepare('
            SELECT
                r.recipe_id AS id
            FROM recipe AS r
            WHERE recipe_id <= 84
            ORDER BY title ASC
        ');
        $statement->execute();
        $recipeIds = $statement->fetchAll();

        $recipes = [];
        foreach ($recipeIds as $recipeIdRow) {
            $recipes[] = $this->getById($recipeIdRow['id']);
        }

        return $recipes;
    }

    /**
     * @return Recipe[]
     */
    public function getFirstThird()
    {
        $statement = $this->db->prepare('
            SELECT
                r.recipe_id AS id
            FROM recipe AS r
            WHERE recipe_id BETWEEN 0 AND 28
            ORDER BY title ASC
        ');
        $statement->execute();
        $recipeIds = $statement->fetchAll();

        $recipes = [];
        foreach ($recipeIds as $recipeIdRow) {
            $recipes[] = $this->getById($recipeIdRow['id']);
        }

        return $recipes;
    }

    /**
     * @return Recipe[]
     */
    public function getSecondThird()
    {
        $statement = $this->db->prepare('
            SELECT
                r.recipe_id AS id
            FROM recipe AS r
            WHERE recipe_id BETWEEN 28 AND 56
            ORDER BY title ASC
        ');
        $statement->execute();
        $recipeIds = $statement->fetchAll();

        $recipes = [];
        foreach ($recipeIds as $recipeIdRow) {
            $recipes[] = $this->getById($recipeIdRow['id']);
        }

        return $recipes;
    }

    /**
     * @return Recipe[]
     */
    public function getLastThird()
    {
        $statement = $this->db->prepare('
            SELECT
                r.recipe_id AS id
            FROM recipe AS r
            WHERE recipe_id BETWEEN 56 AND 84
            ORDER BY title ASC
        ');
        $statement->execute();
        $recipeIds = $statement->fetchAll();

        $recipes = [];
        foreach ($recipeIds as $recipeIdRow) {
            $recipes[] = $this->getById($recipeIdRow['id']);
        }

        return $recipes;
    }


    /**
     * @param $recipeId
     * @return Recipe
     */
    public function getById($recipeId)
    {
        $listOfRecipes = $this->db->prepare('
          SELECT 
            r.recipe_id AS id,
            r.title AS title,
            r.introduction AS intro
          FROM recipe AS r
          WHERE recipe_id = ?');

        $listOfRecipes->execute([$recipeId]);
        $recipeRow = $listOfRecipes->fetch();
        $recipe = new Recipe(
            $recipeRow['id'],
            $recipeRow['title'],
            $recipeRow['intro'],
            $this->getMethods($recipeRow['id']),
            $this->getIngredients($recipeRow['id']),
            $this->getImage($recipeRow['id'])
        );

        return $recipe;
    }

    /**
     * @param $recipeId
     * @return Ingredient[]
     */
    private function getIngredients($recipeId)
    {
        $listOfIngredients = $this->db->prepare('
          SELECT 
            i.recipe_id AS id,
            i.quantity,
            i.unit,
            i.size,
            i.name,
            i.preparation
          FROM ingredients AS i
          WHERE recipe_id = ?');

        $listOfIngredients->execute([$recipeId]);
        $ingredientsArray = $listOfIngredients->fetchAll();
        $ingredients = []; // Creating an array of Ingredient Objects

        foreach ($ingredientsArray as $ingredientRow) {
            $ingredients[] = new Ingredient(
                $ingredientRow['id'],
                $ingredientRow['quantity'],
                $ingredientRow['unit'],
                $ingredientRow['size'],
                $ingredientRow['name'],
                $ingredientRow['preparation']);
        }

        return $ingredients;

    }

    /**
     * @param $recipeId
     * @return Method[]
     */
    private function getMethods($recipeId)
    {
        $listOfMethods = $this->db->prepare('
            SELECT
               m.recipe_id AS id,
               m.method AS method
            FROM method AS m
            WHERE recipe_id = ?');

        $listOfMethods->execute([$recipeId]);
        $methodsArray = $listOfMethods->fetchAll();
        $methods = [];

        foreach ($methodsArray as $methodsRow) {
            $methods[] = new Method($methodsRow['id'], $methodsRow['method']);
        }
        return $methods; // This is an array of Method objects
    }


    /**
     * @param $recipeId
     * @return Image
     */
    public function getImage($recipeId)
    {
        $listOfImages = $this->db->prepare('
            SELECT
               i.recipe_id AS id,
               i.image_name AS name,
               i.image_content AS content
            FROM images AS i
            WHERE recipe_id = ?');

        $listOfImages->execute([$recipeId]);
        $imagesArray = $listOfImages->fetchAll();
        $image = [];


        foreach ($imagesArray as $imageRow) {
            $image[] = new Image($imageRow['id'], $imageRow['name'], $imageRow['content']);
        }
        var_dump($image[0]);

        return $image[0]; // Returns an Image Object


    }

}




































