<?php

namespace thvc\Model;

use thvc\Model\Quantity\Quantity;
use thvc\Model\Quantity\Unit;



class Ingredient
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var Quantity
     */
    private $quantity;
    /**
     * @var Unit
     */
    private $unit;

    private $size;
    private $name;
    private $preparation;

    /**
     * Ingredient constructor.
     * @param $id
     * @param $quantity
     * @param $unit
     * @param $size
     * @param $name
     * @param $preparation
     */
    public function __construct($id, $quantity, $unit, $size, $name, $preparation)
    {
        $this->id = $id;
        $this->quantity = $quantity;
        $this->unit = $unit;
        $this->size = $size;
        $this->name = $name;
        $this->preparation = $preparation;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return mixed
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPreparation()
    {
        return $this->preparation;
    }


}
