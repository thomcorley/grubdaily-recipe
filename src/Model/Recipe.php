<?php

namespace thvc\Model;


class Recipe
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $intro;
    /**
     * @var Method[]
     */
    private $method;
    /**
     * @var Ingredient[]
     */
    private $ingredients;

    /**
     * @var Image
     */
    private $image;

    /**
     * Recipe constructor.
     * @param $id
     * @param $title
     * @param $intro
     * @param Method[] $method
     * @param Ingredient[] $ingredients
     * @param Image $image
     * @internal param Ingredient[] $ingredients
     */
    public function __construct($id, $title, $intro, array $method, array $ingredients, Image $image)
    {
        $this->id = $id;
        $this->title = $title;
        $this->method = $method;
        $this->ingredients = $ingredients;
        $this->intro = $intro;
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Method[]
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return Ingredient[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * @return string
     */
    public function getIntro()
    {
        return $this->intro;
    }
}
