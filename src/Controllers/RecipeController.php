<?php

namespace thvc\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;


class RecipeController
{
    public function recipe(Application $app, $recipeId)
    {
        $recipe = $app['recipe_repository']->getById($recipeId);

        return $app['twig']->render('recipe.html.twig', [
            'recipe' => $recipe,

        ]);
    }

    public function index(Application $app)
    {
        $recipes = $app['recipe_repository']->getAll();
        $recipes1 = $app['recipe_repository']->getFirstThird();
        $recipes2 = $app['recipe_repository']->getSecondThird();
        $recipes3 = $app['recipe_repository']->getLastThird();

        return $app['twig']->render('index.html.twig', [
            'recipes' => $recipes,
            'recipes1' => $recipes1,
            'recipes2' => $recipes2,
            'recipes3' => $recipes3
        ]);
    }
}
