<?php

namespace thvc\Controllers;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController
{
    public function blog(Request $request, Application $app)
    {
        return new Response('Some content');
    }
}
