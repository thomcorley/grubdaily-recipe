<?php

use Silex\Application;
use Symfony\Component\Yaml\Yaml;
use thvc\Bootstrap\Bootstrapper;
use thvc\Database\DbConnection;
use thvc\Model\RecipeRepository;

require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();

$app['env'] = getenv('APP_ENV') ? getenv('APP_ENV') : 'dev';
$app['debug'] = true;

// Controllers

//Bootstrapping
$bootstrapper = new Bootstrapper($app);
$bootstrapper->routingBootstrapper();

//Defining Services

$app['config'] = Yaml::parse(file_get_contents("../config/config_{$app['env']}.yml"));

$app['database_connection'] = function(Application $app) {
    return new DbConnection (
        $app['config']['dbConfig']['dbHost'],
        $app['config']['dbConfig']['dbUser'],
        $app['config']['dbConfig']['dbPass'],
        $app['config']['dbConfig']['dbName'],
        $app['config']['dbConfig']['charset']

    );
};

$app['recipe_repository'] = function(Application $app) {
      return new RecipeRepository($app['database_connection']->newDbConnection());
};



$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__ . '/../resources/views',
]);


$app->get('/', function (Silex\Application $app) {
    // $recipes = $app['recipe_repository']->getRecipes();  this is how we would structure the Twig variable definitions in a real app
    return $app['twig']->render('index.html.twig', [

    ]);
});













$app->run();
